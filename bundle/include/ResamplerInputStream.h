/*
 * Copyright (c) 2021 Todd-AO
 */

#pragma once

#include "Sound.h"

struct SwrContext;

class LIB_API ResamplerInputStream : virtual public SoundInputStream {
private:
	SoundInputStream& in;

	const int desiredSampleRate;
	SwrContext* swrCtx;
	Samples bufferIn;
	Samples bufferOut;

public:
	ResamplerInputStream(SoundInputStream& in, int desiredSampleRate);
	~ResamplerInputStream();

	int getNumChannels() override;

	int getNumSamples() override;

	int getSampleRate() override;

	int getBitDepth() override;

	int read(Samples& samples, int offset, int num) override;

	using SoundInputStream::read;

	void reset() override;

	void seek(int sampleNum) override;
};
