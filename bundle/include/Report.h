/*
 * Copyright (c) 2019 Todd-AO
 */

#pragma once

#include <string>
#include <vector>
#include "config.h"

/**
 * Describes the processing result
 */
class LIB_API Report {
public:
	enum Status {
		NA,
		DONE,
		CANCELED,
		ERROR,
		NONE,
		SKIPPED,
	};

	/**
	 * Describes the Tool processing result
	 */
	struct ToolStatus {
		/**
		 * Tool::id
		 */
		int toolId;

		/**
		 * Processing result Status
		 */
		Status status;

		/**
		 * Error message if Status::ERROR, or preset name if Status::DONE
		 * Displayed in a tooltip in the History window
		 */
		std::string message;
	};

	/**
	 * Do not set by the Processor
	 */
	std::string appVersion;

	/**
	 * Do not set by the Processor
	 */
	std::string dateTime;

	/**
	 * Input file full path
	 */
	std::string inFile;

	/**
	 * Output file full path
	 */
	std::string outFile;

	/**
	 * Full string to display in the History window
	 */
	std::string range;

	/**
	 * Full string to display in the History window
	 */
	std::string channels;

	/**
	 * Status List for selected Tools
	 */
	std::vector<ToolStatus> statusArray;

	Report(const std::string& inFile, const std::string& outFile,
		   int rangeLow, int rangeHigh);

	void set(int toolId, Status status, const std::string& message = "");
};
