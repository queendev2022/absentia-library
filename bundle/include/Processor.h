/*
 * Copyright (c) 2020 Todd-AO
 */

#pragma once

#include <string>
#include <future>
#include <queue>
#include "Batch.h"
#include "FullSamplesStream.h"
#include "Report.h"
#include "Tool.h"
#include "config.h"

class ThreadPool;

class LIB_API Processor {
private:
	const int numThreads;
	std::unique_ptr<ThreadPool> pool;
	std::queue<std::future<void>> waitList;

	std::mutex waitSync;
	std::mutex pauseSync;
	std::condition_variable pauseCondition;

	volatile bool paused;
	volatile bool interrupted;

public:
	/**
	 * numThreads:  number of threads selected in GUI
	 */
	Processor(int numThreads);

	/**
	 * It will call cancel() and wait() before the destruct
	 */
	~Processor();

	/**
	 * Processing function, adds a task to the thread pool
	 *
	 * inFile:         absolute path to input file
	 * outFile:        absolute path to output file, may be the same as inFile
	 * overwrite:      user selected overwrite flag
	 * rangeLow:       >= 0
	 * rangeHigh:      <= RANGE_MAX
	 * keepUID:        user selected
	 * keepProcFile:   user selected
	 * toolsChosen:    array with flags of selected Tools, with size of Tool::getToolset()Size,
	 *                 must be separate for the processing session, not changed with next
	 *                 user processing session
	 * presetsChosen:  user selected Presets, must be separate for the processing session
	 * channelsChosen: user selected channels (may be NULL)
	 * skipped:        GUI sets to true if skipped
	 * updateProgress: function is called in the processing thread to update the GUI
	 *                 progress bar, must return as soon as possible,
	 *                 sends progress value from 0 to PROGRESS_MAX
	 * onComplete:     function is called in the processing thread at the end of processing
	 */
	void process(std::string inFile, std::string outFile, bool overwrite,
				 int rangeLow, int rangeHigh,
				 bool keepUID, bool keepProcFile,
				 const bool* toolsChosen, const Tool::Preset* const* presetsChosen,
				 const bool* channelsChosen,
				 bool& skipped,
				 std::function<void(int val)> updateProgress,
				 std::function<void(Report* report)> onComplete);

	/**
	 * Sample buffer processing, adds a task to the thread pool
	 *
	 * audioBuffer:    the filled input sample buffer will contain the output samples
	 *                 when onComplete function is called
	 */
	void process(FullSamplesStream* audioBuffer,
				 int rangeLow, int rangeHigh,
				 const bool* toolsChosen, const Tool::Preset* const* presetsChosen,
				 const bool* channelsChosen,
				 bool& skipped,
				 std::function<void(int val)> updateProgress,
				 std::function<void(Report* report)> onComplete);

	void process_test(std::string inFile, std::string outFile, bool overwrite,
					  int rangeLow, int rangeHigh,
					  bool keepUID, bool keepProcFile,
					  const bool* toolsChosen, const Tool::Preset* const* presetsChosen,
					  const bool* channelsChosen,
					  bool& skipped,
					  std::function<void(int val)> updateProgress,
					  std::function<void(Report* report)> onComplete);

	void process(Batch& batch);

	/**
	 * Pause processing
	 */
	void pause();

	/**
	 * Resume processing
	 */
	void resume();

	/**
	 * Switch pause / resume, returns true if paused
	 */
	bool switchPause();

	/**
	 * Cancel processing, all started tasks will call onComplete()
	 */
	void cancel();

	/**
	 * Blocks the calling thread until all tasks complete
	 */
	void wait();

	static void afsRunScript();

	static void afsAnalyze(const std::string& inFile, int masterCh);
};
