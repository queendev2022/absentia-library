/*
 * Copyright (c) 2020 Todd-AO
 */

#pragma once

#include <fstream>
#include <string>
#include "WavFile.h"

class LIB_API WavInputStream : virtual public SoundInputStream, virtual public WavFile {
private:
	std::ifstream fileIn;

	std::streampos dataOffset;

	char buffer[BUFFER_SIZE];
	int bufferOffset;
	uint32_t sampleCounter;

	double floatScale;
	double floatOffset;

public:
	WavInputStream(const std::string& path);
	~WavInputStream();

	int read(Samples& samples, int offset, int num);

	using SoundInputStream::read;

	void reset();

	void seek(int sampleNum);

	int64_t readSample();
};
