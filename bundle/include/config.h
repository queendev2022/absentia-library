/*
 * Copyright (c) 2020 Todd-AO
 */

#pragma once

#if defined _WIN32 || defined __CYGWIN__
#	ifdef LIB_EXPORT
#		define LIB_API __declspec(dllexport)
#	else
#		define LIB_API __declspec(dllimport)
#	endif
#else
#	if __GNUC__ >= 4
#		define LIB_API __attribute__((visibility("default")))
#	endif
#endif
#ifndef LIB_API
#	define LIB_API
#endif

/**
 * Upper range value
 */
#define RANGE_MAX 96000

/**
 * The size of channelsChosen array passed to Processor::process
 */
#define NUM_CHANNELS_MAX 16

/**
 * Maximum progress will be returned by updateProgress()
 */
#define PROGRESS_MAX 500
