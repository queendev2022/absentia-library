/*
 * Copyright (c) 2021 Todd-AO
 */

#pragma once

#include <string>
#include "config.h"

/**
 * The namespace contains functions for managing activation
 * ______________
 * The application displays the Activation dialog at startup if !isValid().
 * It suggests 2 methods to activate: using the License Key or iLok License.
 * 
 * If the License Key the next step is the "License Key" field for the 34 character key, 
 * this field does not allow to insert or input chars from "[A-Za-z0-9-]+" regex.
 * The "Activate" button is active if 34 characters are entered, it calls TA activate() function, 
 * shows an error message if TA activate() throws, or goes to Offline Activation step if user agrees.
 * The "# days trial" button is shown if isTrial() and "#" is getTrialDays() value.
 * The "Purchase a License Key" button goes to the browser URL.
 * 
 * iLok License just calls PACE activate() function, it has its own GUI.
 * 
 * The user can use the application if isLaunchAllowed(), otherwise exit.
 * 
 * Please run the PACE activate() process first to use its window size for the ABDX Activation window.
 * ______________
 * The Settings window Help menu has the "Deactivate License" item visible if TA_isValid().
 * It calls deactivate() function.
 * ______________
 * The application displays the Subscription Expired dialog if isValid() && isExpired().
 * This should be checked periodically, the first at startup and after Activation.
 * You can use setCheckCallback() for this.
 * This should pause Processing and resume if isLaunchAllowed() after closing this dialog or exit if not.
 * It suggests Extend or Deactivate.
 * 
 * Extend button goes to the browser URL and show dialog with the Check Again button,
 * that calls checkSubscription() function, and closes the dialog if success.
 * 
 * Deactivate button is visible if TA_isValid(), it calls TA deactivate() function.
 */
namespace activation {

/**
 * The structure is used for offline Activation or Deactivation
 */
struct Offline {
	/**
	 * Should return the path to the user selected offline request file to save,
	 * "skip" string to skip saving, or empty string if canceled
	 * 
	 * default path is "%Documents%/ABDX_ActivationRequest.xml"
	 */
	virtual const std::string getRequestPath() = 0;

	/**
	 * Should return the path to the user selected offline response file to read,
	 * or empty string if canceled
	 * 
	 * default folder is "%Documents%"
	 * 
	 * not used for Deactivation
	 */
	virtual const std::string getResponsePath() = 0;
};

/**
 * The callback function will be called periodically after an internal license check
 * starting from 2nd check
 * 
 * Called in a separate thread
 */
typedef void (*CheckCallback)();

/**
 * Set CheckCallback function
 */
LIB_API void setCheckCallback(CheckCallback checkCallback);

/**
 * Return true if activated
 */
LIB_API bool isValid();

/**
 * Return true if trial
 * 
 * cannot be isValid and isTrial at the same time
 */
LIB_API bool isTrial();

/**
 * Return the number of trial days remaining if isTrial
 */
LIB_API int getTrialDays();

/**
 * Return true if Activation has expired
 * 
 * Activation is still valid
 */
LIB_API bool isExpired();

/**
 * Return expiration date in format "yyyy-MM-dd"
 */
LIB_API const std::string getExpiredDate();

/**
 * Force Subscription check after extension by user
 * 
 * offline:  implemented Offline struct, same as in activate()
 * 
 * return true if Activation has not expired
 */
LIB_API bool checkSubscription(/* Offline& offline */);

enum ActivationCode {
	NOT_REQUIRED = 1,
	SUCCESS = 0,
	KEY_INVALID = -1,
	KEY_INUSE = -2
};

/**
 * Begins the TA Activation process
 * 
 * key:      user entered 34 chars key
 * offline:  implemented Offline struct
 * 
 * return ActivationCode: 
 *        NOT_REQUIRED if TA has already been activated
 *        SUCCESS if TA activated or activation canceled (TA may be not valid)
 *        KEY_INVALID if Invalid product key
 *        KEY_INUSE if The product key has already been activated with the maximum number of computers
 * 
 * may throw std::runtime_error with a message
 */
LIB_API int activate(const std::string& key, Offline& offline);

/**
 * Begins the PACE Activation process
 * 
 * return true if PACE activated
 */
LIB_API bool activate();

/**
 * Begins the TA Deactivation process
 * 
 * offline:  implemented Offline struct
 * 
 * return true if TA deactivated
 * 
 * may throw std::runtime_error with a message
 */
LIB_API bool deactivate(Offline& offline);

/**
 * Begins the PACE Deactivation process
 * 
 * return true if PACE deactivated
 */
LIB_API bool deactivate();

/**
 * Return true if TA activated
 */
LIB_API bool TA_isValid();

/**
 * Return true if PACE activated
 */
LIB_API bool PACE_isValid();

/**
 * Return number of hours before TA expiration
 */
LIB_API int TA_getExpirationHours();

/**
 * Return number of hours before PACE expiration
 */
LIB_API int PACE_getExpirationHours();

/**
 * Return TA License key if activated
 */
LIB_API const std::string TA_getKey();

/**
 * Return true if the application can be launched
 */
inline bool isLaunchAllowed() {
	return isValid() && !isExpired() || isTrial();
}

}  // namespace activation
