/*
 * Copyright (c) 2020 Todd-AO
 */

#pragma once

#include <string>
#include <vector>
#include "Report.h"
#include "Tool.h"
#include "config.h"

class LIB_API Batch {
public:
	class ProcessCtx {
	public:
		virtual bool isSkipped() = 0;

		virtual void onProgress(int val) = 0;

		virtual void onComplete(Report* report) = 0;
	};

	std::vector<std::string> inFiles;
	std::string outFolder;
	std::string prefix;
	std::string suffix;

	bool overwrite = false;

	int rangeLow = 0;
	int rangeHigh = RANGE_MAX;

	bool keepUID = true;
	bool keepProcFile = false;

	// const bool* toolsChosen;
	std::vector<bool> toolsChosen;
	// const Tool::Preset* const* presetsChosen;
	std::vector<Tool::Preset*> presetsChosen;
	// const bool* channelsChosen;
	std::vector<bool> channelsChosen;

	Batch();
	
	virtual ProcessCtx& processStarted(const std::string& file) = 0;
};
