/*
 * Copyright (c) 2022 Todd-AO
 */

#pragma once

#include <functional>
#include <string>
#include <map>
#include <vector>
#include <mutex>

#include "config.h"
#include "Progress.h"
#include "Sound.h"
#include "Channels.h"

/**
 * Describes the Tool and its Presets
 */
class LIB_API Tool {
public:
	/**
	 * Describes Preset
	 */
	class LIB_API Preset {
	public:
		typedef std::map<const std::string, std::string> Props;

		/**
		 * Describes custom Preset variables
		 */
		class LIB_API Value {
		public:
			enum Type {
				Checkbox,
				Slider
			};

			/**
			 * Value name
			 */
			const std::string name;

			/**
			 * Displays in tooltip
			 */
			const std::string hint;

			/**
			 * GUI element Type
			 */
			const Type type;

			/**
			 * Minimum value
			 */
			const double min;

			/**
			 * Maximum value
			 */
			const double max;

			/**
			 * Value decimal part to display
			 * 1 is 0 decimal places,
			 * 10 is 1 decimal places,
			 * etc.
			 */
			const int decimal;

			/**
			 * If true, then the values should be displayed in a dB scale
			 * min, max, get() must be converted to dB: val_dB = 20 * log10(val)
			 * The value for set() must be converted back to linear: val = pow(10, val_dB / 20)
			 */
			const bool dBScale;

			/**
			 * Returns the Preset value as double
			 */
			std::function<double(Tool::Preset* preset)> get;

			/**
			 * Set double value to Preset
			 */
			std::function<void(Tool::Preset* preset, double val)> set;

			Value(const std::string& name, const std::string& hint, Type type,
				  std::function<double(Tool::Preset*)> get,
				  std::function<void(Tool::Preset*, double)> set,
				  double min = 0, double max = 0, int decimal = 0, bool dBScale = false);
		};

		/**
		 * Preset name used in drop down list
		 */
		std::string name;

		std::string path;
	};

	struct ProcessCtx {
		class LIB_API SkippedException;

		const Preset* preset;
		std::string inFile;
		std::string outFile;
		std::string tmpFile;
		bool keepUID;
		int rangeLow;
		int rangeHigh;
		std::unique_ptr<const Channels> channels;
		SoundInputStream* iStream;
		std::function<SoundOutputStream*()> oStream;
		Progress progress;
		std::function<void()> checkSkipped;
		std::mutex& fs_mutex;
		int numThreads;

		bool detected;
	};

	/**
	 * List of Tool implementations
	 */
	static std::vector<Tool*> toolset;

	/**
	 * Constant unique Tool id
	 */
	const int id;

	/**
	 * Tool name
	 */
	const std::string name;

	const std::string nameShort;

	/**
	 * Tool Presets list
	 * No function adds or removes a user Preset to this list
	 */
	std::vector<Preset*> presets;

	/**
	 * List of Tool Preset Values available for change by user
	 */
	std::vector<const Preset::Value*> presetValues;

	/**
	 * Index of the Preset selected on first start
	 */
	const int presetDefIndex;

	/**
	 * Number of predefined Presets at the top of presets list
	 */
	int numDefPresets;

	const bool keepProcFile;

protected:
	Tool(int id, const std::string& name, bool keepProcFile = true, int presetDefIndex = 0);

	virtual Preset* loadPreset(Preset::Props& props);
	
	virtual void savePreset(Preset* preset, Preset::Props& props);

public:

	/**
	 * Get list of Tool implementations
	 */
	static std::vector<Tool*>& getToolset();

	/**
	 * Get Tool from toolset by id
	 */
	static Tool* getById(int id);

	/**
	 * Add all user Presets from presetsPath folder
	 * Current path is "%UserHome%/.absentiadx/presets"
	 * Must be called before working with the library
	 */
	static void loadPresets(const std::string& presetsPath);

	/**
	 * Copy this Tool Preset to start changes
	 */
	virtual Preset* copyPreset(Preset* preset);

	/**
	 * Save this Tool user Preset in presetsPath folder
	 */
	void savePreset(Preset* preset);

	/**
	 * Delete this Tool user Preset file
	 */
	void removePreset(Preset* preset);

	virtual void process(ProcessCtx& ctx) const = 0;
};
