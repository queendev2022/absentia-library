/*
 * Copyright (c) 2020 Todd-AO
 */

#pragma once

#include <atomic>
#include <functional>
#include <future>
#include <mutex>
#include <vector>
#include <list>
#include "Sound.h"

/**
 * Provides parallel rendering of the sonogram and waveform parts in different scales
 */
class LIB_API Sonogram : virtual public Sound {
public:
	/**
	 * Corresponds to 4-byte Qt color format
	 * QImage::Format_RGB32
	 * QImage::Format_ARGB32
	 */
	union Color {
		struct {
			unsigned char b, g, r, a;
		} v;

		struct {
			unsigned int rgb : 24;
			unsigned int a : 8;
		} z;

		unsigned int argb;
	};

	/**
	 * Sonogram Image type
	 * pixels array format in the left-to-right, top-to-bottom order
	 */
	struct Image {
		Color* pixels;
		const int w, h;

		Image(int w, int h);
		~Image();

		inline Color* operator[](int x) {
			return pixels + x * h;
		}
	};

	/**
	 * Number of render threads
	 */
	static const int numRenderThreads;

private:
	std::list<std::future<void>> queue;
	std::mutex syncQueue;

protected:
	static const Color* colormap;
	static int colormapSize;

	const int imageWidth;

	std::shared_ptr<bool> stop;

private:
	SoundInputStream* iStream;
	std::mutex syncRead;
	int position;

	std::atomic_bool* ctxArray;
	std::vector<Image*> createdImages;

protected:
	Sonogram(const std::string& path, int imageWidth);

public:
	/**
	 * Returns a new Sonogram instance
	 * 
	 * path:              absolute path to input file
	 * imageWidth:        width of rendered images
	 */
	static Sonogram* getSonogram(const std::string& path, int imageWidth);

	virtual ~Sonogram();

	/**
	 * Returns the number of audio channels
	 */
	int getNumChannels();

	/**
	 * Returns the number of samples per audio channel
	 */
	int getNumSamples();

	/**
	 * Returns the number of samples per second of audio
	 */
	int getSampleRate();

	int getBitDepth();

	/**
	 * Returns an array of channel names
	 */
	std::string* getChannelNames();

	/**
	 * Reads audio into samples array
	 * 
	 * sampleNum:         audio file offset in samples
	 * samples:           output samples buffer
	 * offset:            samples buffer offset in samples
	 * num:               number of samples to read
	 */
	int read(int sampleNum, Samples& samples, int offset, int num);

	/**
	 * Returns a frequency corresponding to the image coordinate y
	 */
	virtual int convertYtoFreq(int y);

	/**
	 * Returns the height of the rendered image
	 */
	virtual int getImageHeight() = 0;

	/**
	 * Returns the total number of frames (horizontal pixels) for the highest horizontal resolution (zoom = 1)
	 */
	virtual int getNumFramesBase() = 0;

	/**
	 * Creates a stream for rendering Sonogram image
	 * the result of dividing the number of frames by imageWidth should be a power of 2
	 * 		zoom = (endFrame - startFrame) / imageWidth
	 * 		assert(zoom != nextPow2(zoom))
	 * 
	 * startFrame:        start frame for base scale
	 * endFrame:          end frame for base scale, excluding
	 * event:             event will be called asynchronously after rendering
	 */
	void renderSonogram(int startFrame, int endFrame, std::function<void(Image* images)> event);

	/**
	 * Creates a stream for rendering Waveform image
	 * the result of dividing the number of frames by imageWidth should be a power of 2
	 * 
	 * startFrame:        start frame for base scale
	 * endFrame:          end frame for base scale, excluding
	 * event:             event will be called asynchronously after rendering
	 */
	void renderWaveform(int startFrame, int endFrame, std::function<void(Image* images)> event);

	/**
	 * Cancel all render streams, so the Sonogram is ready to accept new ones
	 * does not block the calling thread
	 * canceled events will not be called
	 */
	void cancel();

	/**
	 * Blocks the calling thread until all renders are complete
	 */
	void wait();

protected:
	virtual Image* renderSonogram(int startFrame, int endFrame, int ctxId, bool& stop) = 0;

	virtual Image* renderWaveform(int startFrame, int endFrame, int ctxId, bool& stop) = 0;

	Image* makeImages(int w, int h);
};

typedef Sonogram::Color SGColor;
typedef Sonogram::Image SGImage;
