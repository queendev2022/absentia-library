/*
 * Copyright (c) 2019 Todd-AO
 */

#pragma once

#include "Sound.h"

class LIB_API FullSamplesStream : virtual public SoundInputStream, virtual public SoundOutputStream {
private:
	int numChannels;
	int numSamples;
	int sampleRate;
	int bitDepth;
	int rPos;
	int wPos;

public:
	/**
	 * The sample buffer is initialized, can be read / written directly
	 */
	Samples samples;

	FullSamplesStream(Sound& sound);

	FullSamplesStream(int numChannels, int numSamples, int sampleRate);

	int getNumChannels();

	int getNumSamples();

	int getSampleRate();

	int getBitDepth();

	int read(Samples& samples, int offset, int num);

	int write(Samples& samples, int offset, int num);

	using SoundInputStream::read;
	using SoundOutputStream::write;

	void reset();

	void seek(int sampleNum);
};
