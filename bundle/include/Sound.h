/*
 * Copyright (c) 2020 Todd-AO
 */

#pragma once

#include <string>
#include "config.h"

/**
 * Wraps samples buffer for reading and writing
 */
struct LIB_API Samples {
	double** const buffer;
	const int numChannels;
	const int size;

	Samples(int numChannels, int size = 0);
	~Samples();

	inline double* operator[](int ch) {
		return buffer[ch];
	}

	inline const double* operator[](int ch) const {
		return buffer[ch];
	}
};

class LIB_API Sound {
public:
	virtual int getNumChannels() = 0;

	virtual int getNumSamples() = 0;

	virtual int getSampleRate() = 0;

	virtual int getBitDepth() = 0;

	virtual bool hasToolFlag(int toolId);

	virtual int* getChannelIndexes();

	virtual std::string* getChannelNames();

	static bool checkTrial(const std::string& path);

	static bool checkFormat(const std::string& path);

	static bool isWav(const std::string& path);
};

class LIB_API SoundInputStream : virtual public Sound {
public:
	virtual ~SoundInputStream() = default;

	virtual int read(Samples& samples, int offset, int num) = 0;

	virtual int read(Samples& samples);

	virtual void reset() = 0;

	virtual void seek(int sampleNum) = 0;
};

class LIB_API SoundOutputStream : virtual public Sound {
public:
	virtual ~SoundOutputStream() = default;

	virtual int write(Samples& samples, int offset, int num) = 0;

	virtual int write(Samples& samples);
};
