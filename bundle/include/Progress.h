/*
 * Copyright (c) 2019 Todd-AO
 */

#pragma once

#include <functional>

#include "config.h"

class LIB_API Progress {
private:
	const float progressPerCnt;
	int cnt;
	int progress;
	int progressMax;
	int value;

	std::function<void(int val)> updateProgress;

public:
	Progress(int num, int max, std::function<void(int val)> updateProgress);

	void init(int max);
	void update(int incr = 1);
};
