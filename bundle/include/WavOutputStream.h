/*
 * Copyright (c) 2020 Todd-AO
 */

#pragma once

#include <fstream>
#include <string>
#include "WavFile.h"

class LIB_API WavOutputStream : virtual public SoundOutputStream, virtual public WavFile {
private:
	// using Convert = int64_t (WavOutputStream::*)(double);
	// using Convert = int64_t (*)(WavFile&, double);

	std::ofstream fileOut;

	std::streampos dataOffset;

	char buffer[BUFFER_SIZE];
	int bufferOffset;

	double floatScale;
	double floatOffset;

	int channelNum;
	bool keepUID;

public:
	WavOutputStream(const std::string& path, const WavFile& base, int toolId = -1, bool keepUID = true, int channelNum = -1);

	WavOutputStream(const std::string& path, int numChannels = 1, int sampleRate = 48000, int bitDepth = 24, bool floatFormat = false, int toolId = -1);

	~WavOutputStream();

	int write(Samples& samples, int offset, int num);

	using SoundOutputStream::write;

	void writeSample(int64_t sample);

private:
	void writeHeader();

	void writeChunks(bool trailing);
};
