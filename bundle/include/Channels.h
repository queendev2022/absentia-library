/*
 * Copyright (c) 2021 Todd-AO
 */

#pragma once

#include <string>
#include <vector>

#include "config.h"
#include "Sound.h"

class LIB_API Channels {
public:
	static const std::vector<bool> first;
	static const std::vector<bool> all;

private:
	std::vector<bool> selected;
	int numSelected;

public:
	Channels(Sound& sound, const std::vector<bool>& channels = {}, bool fromMetadata = false);

	Channels(Sound& sound, const bool* channels = NULL, bool fromMetadata = false) :
		Channels(sound, std::vector<bool>(channels, channels + NUM_CHANNELS_MAX), fromMetadata) {
	}

	bool operator[](size_t ch) const {
		return selected[ch];
	}

	int getNum() const {
		return numSelected;
	}

	std::string toString() const;
};
