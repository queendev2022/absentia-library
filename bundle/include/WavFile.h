/*
 * Copyright (c) 2021 Todd-AO
 */

#pragma once

#include "Sound.h"
#include <vector>
#include <set>
#include <memory>

#define BUFFER_SIZE 32768

struct CueMarker {
	uint32_t start;
	uint32_t end;
	std::string label;
};

bool operator<(const CueMarker& a, const CueMarker& b);

class LIB_API CueMarkers : public std::set<CueMarker> {
public:
	void addMarker(unsigned position, const std::string& label = "");

	void addRegion(unsigned start, unsigned end, const std::string& label = "");
};

class LIB_API WavFile : virtual public Sound {
public:
	class LIB_API Chunk {
	public:
		union LIB_API ID {
			char st[4];
			int32_t dg;

			ID();
			ID(const char st[5]);

			bool operator==(const ID&) const;
		};

		struct Header {
			ID id;
			uint32_t size;
		};

		Header header;
		char* data;
		uint32_t dataSize;
		bool trailing;

		Chunk(const Header& header, char* data, uint32_t dataSize, bool trailing);
		virtual ~Chunk();

		virtual void clearUID();

		virtual Chunk* getForChannel(int channelNum);

		virtual int* getChannelIndexes(int numCh);

		virtual std::string* getChannelNames(int numCh);
	};

protected:
	struct Format {
		uint16_t audioFormat;
		uint16_t numChannels;
		uint32_t sampleRate;
		uint32_t byteRate;
		uint16_t blockAlign;
		uint16_t bitsPerSample;
	};

	Format format;
	std::vector<char> formatEx;
	bool floatFormat;
	int bytesPerSample;
	uint32_t toolFlags;
	uint32_t numSamples;

	std::shared_ptr<std::vector<Chunk*>> chunks;
	Chunk* cueChunk;

	WavFile();

	WavFile(int numChannels, int sampleRate, int bitDepth, bool floatFormat);

	virtual ~WavFile();

public:
	int getNumChannels();

	int getNumSamples();

	int getSampleRate();

	int getBitDepth();

	bool hasToolFlag(int toolId);

	int* getChannelIndexes();

	std::string* getChannelNames();

	Chunk* getChunk(const Chunk::ID& id);

	void addChunk(const Chunk::Header& header, char* data, uint32_t dataSize, bool trailing);

	CueMarkers getMarkers();

	void setMarkers(const CueMarkers& markers);
};
