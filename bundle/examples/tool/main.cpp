/*
 * Copyright (c) 2022 Todd-AO
 */

#include <iostream>
#include <functional>

#include "Transcribe.h"
#include "WavInputStream.h"

int main(int argc, const char* argv[]) {
	const Transcribe transcribe;

	const std::string inFile = "/Volumes/VMware Shared Folders/Shared/samples/CC11A006.wav";

	WavInputStream iStream(inFile);

	auto onProgressUpdate = [](int val) {
		std::cout << val << std::endl;
	};

	auto checkSkipped = [] {
	};

	std::mutex fs_mutex;

	Tool::ProcessCtx ctx{
		.preset = transcribe.presets[0],
		.inFile = inFile,
		.tmpFile = inFile + "_out.wav",
		.iStream = &iStream,
		.progress = Progress(1, 20, onProgressUpdate),
		.checkSkipped = checkSkipped,
		.fs_mutex = fs_mutex,
		.detected = true
	};

	std::cout << "start process" << std::endl;

	transcribe.process(ctx);

	std::cout << "done" << std::endl;
}
