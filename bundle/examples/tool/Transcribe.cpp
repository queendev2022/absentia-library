/*
 * Copyright (c) 2022 Todd-AO
 */

#include "Transcribe.h"
#include "WavInputStream.h"
#include "WavOutputStream.h"

void Transcribe::process(ProcessCtx& ctx) const {
	const int numChannels = ctx.iStream->getNumChannels();
	const int numSamples = ctx.iStream->getNumSamples();

	if (!Sound::isWav(ctx.inFile)) {
		ctx.detected = false;
		return;
	}

	WavInputStream* iWavStream = dynamic_cast<WavInputStream*>(ctx.iStream);

	// std::unique_ptr<const std::string[]> channelNames(iWavStream->getChannelNames());

	// Getting iXML chunk
	WavFile::Chunk* ixmlChunk = iWavStream->getChunk(WavFile::Chunk::ID("iXML"));

	// Create new chunk data
	// The new size must be an even number
	const size_t newDataSize = 1024;

	char* newData = new char[newDataSize]();
	strcpy(newData, "Test ixml data");

	if (ixmlChunk) {
		// Swapping the chunk data
		delete[] ixmlChunk->data;

		ixmlChunk->data = newData;

		// Update ixmlChunk->dataSize and ixmlChunk->header.size if the data size is changed
		ixmlChunk->dataSize = newDataSize;
		ixmlChunk->header.size = newDataSize;
	} else {
		// Adding a chunk
		iWavStream->addChunk(WavFile::Chunk::Header{"iXML", newDataSize}, newData, newDataSize, false);
	}

	// Now you can open the output WAV stream
	std::unique_ptr<WavOutputStream> oWavStream(new WavOutputStream(ctx.tmpFile, *iWavStream, id, true));

	// Reset the input stream if you have read it before
	// iWavStream->reset();

	// And copy the interleaved data
	for (int i = 0; i < numSamples; i++) {
		for (int ch = 0; ch < numChannels; ch++) {
			oWavStream->writeSample(iWavStream->readSample());
		}
	}

	/* ctx.progress.init(numSamples / 2048);

	Samples buffer(numChannels, 2048);
	
	for (int size = 1; size > 0; ) {
		ctx.checkSkipped();
		ctx.progress.update();

		size = wavStream->read(buffer);
	} */
}
