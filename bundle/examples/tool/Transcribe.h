/*
 * Copyright (c) 2022 Todd-AO
 */

#pragma once

#include "Tool.h"

class Transcribe : public Tool {
public:
	class Preset : public Tool::Preset {
	public:
		int formatId;
		int langId;
	};

	Transcribe() : Tool(10, "Transcribe") {
		presets = {
			new Preset{{"WavFileName.xls"}, 0, 0},
			new Preset{{"ProjectName.xls"}, 1, 0},
		};

		presetValues = {
			new Preset::Value("Language",
							  "",
							  Preset::Value::Type::Slider,
							  [](Tool::Preset* preset) { return ((Transcribe::Preset*)preset)->langId; },
							  [](Tool::Preset* preset, double val) { ((Transcribe::Preset*)preset)->langId = (int)val; },
							  0, 65535, 1),
		};
	}

	void process(ProcessCtx& ctx) const;
};
